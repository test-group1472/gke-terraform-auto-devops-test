resource "google_container_cluster" "review" {
  name                     = var.review_cluster_name
  location                 = var.gcp_region
  remove_default_node_pool = true
  initial_node_count       = 1
  description              = var.review_cluster_description
}

resource "google_container_node_pool" "review_nodes" {
  name       = "${var.review_cluster_name}-node-pool"
  cluster    = google_container_cluster.review.name
  location   = var.gcp_region
  node_count = var.node_count

  node_config {
    preemptible  = false
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
